## Разработать скрипт в виде обычного приложения (с использованием метода main():

1. Войти в Админ Панель
2. Выбрать пункт меню `Каталог -> категории` и дождаться загрузки страницы управления категориями.
3. Нажать `«Добавить категорию»` для перехода к созданию новой категории.
4. После загрузки страницы ввести название новой категории и сохранить изменения. На странице управления категориями должно появиться сообщение об успешном создании категории.
5. Отфильтровать таблицу категорий по имени и дождаться там появления записи созданной категории.

|Дополнительно необходимо описать логгер, который будет выводить в консоль базовые действия драйвера в процессе выполнения скрипт (переход на страницу, поиск элементов, клики по элементам). Использовать свою реализацию WebDriverEventListener.|
-------------------------
-------------------------