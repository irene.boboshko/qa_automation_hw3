package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

class ScreenshotMaker {
    void makeScreenshot(WebDriver driver, String screenshotName) {
        TakesScreenshot screenshot = (TakesScreenshot)driver;
        File source = screenshot.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(source, new File("./src/test/java/logs/screenshots/" + screenshotName + ".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
